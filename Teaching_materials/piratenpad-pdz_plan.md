Link to the original file: https://piratenpad.de/p/pdz_plan

## PDZ meeting

#### Trainers: Toby Gibson, Matt Rogon (rogon@embl.de), Marc Gouw, Jelena Calyseva, Malvika Sharan

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Add your comments here:

- Comments by the Participants:
    - I really like the cytoscape course but due to time constrains I couldn`t able to take full advantage of the course. I am quite sure that I will be using it in near future. So it would be very nice of you if you can share some basic as well as advance course material on cytoscape.

- Response by the tutors: 
    - Please see the files here for CytoScape tutorials: https://git.embl.de/rogon/introduction_to_cytoscape

- Comments by the Participants:
    - If you have some courses in Kegg and Reactome patheway analysis, then that would be very useful to me.

- Response by the tutors: 
    - Please see the docs here for Reactome and other related materials: https://git.embl.de/rogon/Monterotondo_Module_2/tree/master/Practical/3.%20ReactomeFI

#### Advanced bioinformatic courses: I can direct you to different learning resources for this

- Comments by the Participants:
    - I wish to learn R course in details, so if you have some basic and advance course material then please share it.

- Response by the tutors: 
    - http://rstatisticsguide.com/the-best-free-courses-to-learn-r/
    - https://www.coursera.org/learn/r-programming

- Comments by the Participants:
    - Analysis of High-Throughput Sequencing Data and RNA seq analysis

- Response by the tutors: 
    - https://www.ebi.ac.uk/training/online/course/embo-practical-course-analysis-high-throughput-seq
    - https://www.ebi.ac.uk/training/events/2017/analysis-high-throughput-sequencing-data-0

#### This is not in the scope of my expertise but I can look around for some materials

- Comments by the Participants:
    - can you post some reading material for phosphositeplus?

- Response by the tutors: 
    - We do not have a self developed tutorial for this.
    - https://www.youtube.com/watch?v=lJ1BxYTAqzQ
    - https://www.phosphosite.org/homeAction.action

- Comments by the Participants:
    - Some course materials for Drug repurposing

- Response by the tutors: 
    - We do not have a self developed tutorial for this.

#### Online materials for different biological themes:
- https://fairsharing.org/

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- Tools for 3D structure prediction: 
-- Phyre2 (personal preference)
-- other tools: https://molbiol-tools.ca/Protein_tertiary_structure.htm

- Phospho site prediction: 
-- NetPhos: http://www.cbs.dtu.dk/services/NetPhos/

### Notes during the course:

This might lose its format, please see th priginal piratenpad

#### Unix Materials

Material: https://github.com/malvikasharan/SWC_reference_material/blob/master/Unix_Shell/Unix_Shell.md
Origin of Species (example document): https://github.com/malvikasharan/SWC_reference_material/blob/master/Unix_Shell/origin_of_species.txt

#### Shared by Malvika

Blast result: http://www.uniprot.org/blast/uniprot/B20170926A7434721E10EE6586998A056CCD0537E0410ABM
clustal omega result: http://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20170926-155909-0079-57062684-pg
emboss server: http://emboss.bioinformatics.nl/

SOS_human Interpro result: 
    https://www.ebi.ac.uk/interpro/sequencesearch/iprscan5-S20170927-140201-0896-11166200-p2m

Domains and disorders: 
    IUPred guide: http://iupred.enzim.hu/Help.php 
    Anchor guide: http://anchor.enzim.hu/Help.php
    TMHMM guide: http://www.cbs.dtu.dk/services/TMHMM/TMHMM2.0b.guide.php
    
    Exercises:
    https://docs.google.com/document/d/1S_gMSr3P6C_qk9lQ7IH0F04E4n7iFLXsoOmpzJ45Xpk/edit#heading=h.1ponmc33dg79

#### Shared by Marc 

ELM practical material: https://docs.google.com/document/d/11T4wjfB0mOA6JTJSKKCIvSqmXLWhfjeXvLWnGV881gU/edit?usp=sharing

#### From Lena

- http://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20170927-134403-0253-4849516-pg

>DLG4
RIVIHRGSTGLGFNIVGGEDGEGIFISFILAGGPADLSGELRKGDQILSVNGVDLRNASH
EQAAIALKNAGQTVTIIAQYK
>SHANK1
TVLLQKKDSEGFGFVLRGAKAQTPIEEFTPTPAFPALQYLESVDEGGVAWRAGLRMGDFL
IEVNGQNVVKVGHRQVVNMIRQGGNTLMVKVVMVT

- Jalview tutorials:

    - https://docs.google.com/document/d/1ceyNSXCpytsG0Bih-sRIOKnf2ZRNxJTOp9jCzKn6lOY/edit#heading=h.p0bkrpqyf7n3
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#### Computer requirements:


Marc Gouw, Malvika Sharan, Jelena Calyseva (unix and basic protein bioinformatics):
Git bash: https://git-for-windows.github.io/


Toby Gibson (Jalview and Chimera): 
    Java-8: http://www.oracle.com/technetwork/java/javase/8-whats-new-2157071.html
    Jalview: http://www.jalview.org/download
    
Day - 3

Matt Rogon (Cytoscape)
    Java-8: http://www.oracle.com/technetwork/java/javase/8-whats-new-2157071.html
    Cytoscape3.5.x (not 3.6): http://www.cytoscape.org/download.php

Toby
    Chimera-2 (not x): https://www.cgl.ucsf.edu/chimera/download.html

Jelena Calyseva:
No specific 

Michael Kuhn (String)
No specific requirement

Other important links:

Program: https://docs.google.com/document/d/1pfJiuC3m3WYMshiX0m6vqvVJwxhMqIO_3Ny9iK6pvew/edit?usp=sharing
    
Please add your preferences for the core facility tour (October 2nd): 
https://docs.google.com/spreadsheets/d/1LEog9biDugmybsvNoNXwwQ8-7ndXPl1n34U2hV_PhwM/edit?usp=drive_web

Dinner preferences: https://docs.google.com/spreadsheets/d/1zpV-mU59fQ-kNpreph48ogQdOm29MyrgGQan_g3N0B0/edit
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Prerequisites:

Materials: The teaching materials will be shared on the day of workshops and will be made available online.

Reading recommendations for pre-workshop preparation:

Multiple sequence alignment:
Jalview 2:
https://www.ncbi.nlm.nih.gov/pubmed/19151095
MSA theory
http://www.sciencedirect.com/science/article/pii/0022283686902524?via%3Dihub

Protein-protein interaction:
String database:
https://www.ncbi.nlm.nih.gov/pubmed/27924014

Short linear motifs:
Eukaryotic Linear Motif database
https://www.ncbi.nlm.nih.gov/pubmed/26615199


Optional materials:

Reviews on PDZ:
PDZ domains and their binding partners: structure, specificity, and modification
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2891790/
A structural portrait of the PDZ domain family.
https://www.ncbi.nlm.nih.gov/pubmed/25158098 (abstract)
http://www.sciencedirect.com/science/article/pii/S0022283614004318?via%3Dihub (full)

Protein related papers:
sequence to structure and function--current status.
https://www.ncbi.nlm.nih.gov/pubmed/20887265 (Abstract)
http://arep.med.harvard.edu/johnson/predict/protein_main.html
http://www.biochem.ucl.ac.uk/bsm/dbbrowser/jj/

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------


