## Other course materials 

### The links below will direct you to the external files

1. *JalView*, *Chimera* and otrher exercises by Toby Gibson, Marc Gouw and Malvika Sharan:
https://docs.google.com/document/d/1ceyNSXCpytsG0Bih-sRIOKnf2ZRNxJTOp9jCzKn6lOY/edit?usp=sharing

2. *Cytoscape* lesson and exercises by Matt Rogon:
https://git.embl.de/rogon/introduction_to_cytoscape

3. *Unix* Course by Marc Gouw and Malvika Sharan:
https://github.com/malvikasharan/SWC_reference_material/blob/master/Unix_Shell/Unix_Shell.md

4. *String* and *Stitch* Exercises by michael Kuhn:
https://www.dropbox.com/sh/zfad20wfa8t485j/AACQl76GoP_9x0DP55tOKh5ka?dl=0